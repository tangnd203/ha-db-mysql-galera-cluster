# High Availability Database (MySQL Galera Cluster)
## 1 - What is Galera Cluster ?
- Galera Cluster là một plug-in sao chép multi-master đồng bộ cho InnoDB
- MySQL Galera Cluster là giải pháp phổ biến cho tính sẵn sàng cao của MySQL và mang lại tỷ lệ tồn tại cao cho cơ sở dữ liệu cao nhất (99,99...)
## 2 - Galera Cluster Architecture
![alt text](images/image1.png)
- Database Management System :  là database server chạy trên các node riêng lẻ (MySQL Server, Percona Server cho MySQL và MariaDB Server)
- wsrep API :  là giao diện của database server và là nhà cung cấp bản sao
  + wsrep Hooks :  Công cụ này tích hợp với công cụ máy chủ cơ sở dữ liệu để sao chép tập hợp ghi
  + dlopen() : Hàm này làm cho nhà cung cấp wsrep có sẵn cho các wsrep hook
- Galera Replication Plugin : Plugin này cho phép chức năng dịch vụ sao chép tập hợp ghi
- Group Communication plugins : Có một số hệ thống liên lạc nhóm có sẵn cho Cụm Galera (ví dụ: gcomm và Spread)
## 3 - How does Galera Cluster work ?
![alt text](images/image2.png)
a - Synchronous Replication
- Khi bất kỳ nút nào nhận được thao tác ghi, nó sẽ áp dụng thay đổi cho công cụ lưu trữ InnoDB cục bộ của nó và sau đó phát thay đổi đó tới tất cả các nút khác trong cụm
- Tất cả các nút khác đều nhận được thay đổi, áp dụng nó cho công cụ lưu trữ InnoDB cục bộ của chúng và xác nhận việc nhận lại nút gốc
- Khi tất cả các nút đã xác nhận đã nhận được thay đổi, nút gốc sẽ thực hiện giao dịch
b - Cluster Communication
- Galera Cluster sử dụng giao thức gossip để liên lạc giữa các nút => mỗi nút định kỳ gửi trạng thái của nó đến tất cả các nút khác trong cụm => đảm bảo tất cả các nút luôn cập nhật những thay đổi mới nhất
c - Flow Control
- Galera Cluster sử dụng cơ chế kiểm soát luồng để tránh mất dữ liệu. Cơ chế này đảm bảo việc ghi được áp dụng cho tất cả các nút trong cụm theo cùng một thứ tự
- Nếu một nút bị tụt lại phía sau, các nút khác sẽ làm chậm quá trình ghi của chúng cho đến khi nút chậm bắt kịp
d - Failover
- Nếu một nút bị lỗi, các nút khác sẽ tự động bầu ra một người lãnh đạo mới =>  đảm bảo luôn có sẵn một nút để chấp nhận ghi
- Client có thể tiếp tục kết nối với bất kỳ nút nào trong cụm, ngay cả sau khi chuyển đổi dự phòng
## 4 - Deploying a Galera Cluster with ClusterControl
![alt text](images/image3.png)
a - Chuẩn bị
- 4 server (ubuntu server 20.04)
  + ClusterControl : 192.168.188.142
  + ClusterControl 1 : 192.168.188.144
  + ClusterControl 2 : 192.168.188.145
  + ClusterControl 3 : 192.168.188.146
- Username và Password của các server giống nhau 
b - Install ClusterControl
```
    $ sudo apt update
    $ wget http://www.severalnines.com/downloads/cmon/install-cc
    $ chmod +x install-cc
    $ sudo ./install-cc
```
c - Thiết lập SSH
```
    $ ssh-keygen -t rsa
    $ ssh-copy-id 192.168.188.142 # clustercontrol
    $ ssh-copy-id 192.168.188.144 # galera1
    $ ssh-copy-id 192.168.188.145 # galera2
    $ ssh-copy-id 192.168.188.146 # galera3
```
d - Mở các port cho ClusterControl
```
    $ sudo ufw enable
    $ sudo ufw ssh allow
    $ sudo ufw http allow
    $ sudo ufw https allow
    $ sudo ufw 3306 allow
    $ sudo ufw 4444 allow
    $ sudo ufw 4567 allow
    $ sudo ufw 4568 allow
```
e - Cluster Deployment
- General and SSH Settings
![alt text](images/image4.png)
- Node configuration
![alt text](images/image5.png)
- Add Node MySQL server
![alt text](images/image6.png)
- Preview
![alt text](images/image7.png)
- Dashboards
![alt text](images/image8.png)

f - Accessing the Galera Cluster
![alt text](images/image9.png)
- Reverse Proxy
  + HAProxy : hoạt động tương tự như bộ chuyển tiếp TCP (hoạt động trong lớp Transport của mô hình TCP/IP)
  + ProxySQL (2015) : 
    * chấp nhận lưu lượng truy cập từ MySQL client và chuyển tiếp đến backend MySQL server
    * hỗ trợ các cấu trúc liên kết MySQL replication khác nhau và thiết lập mutil-server với Galera Cluster với các khả năng như query routing, sharding, queries rewrite, query mirroring,...
  + MariaDB MaxScale : là một database proxy cho phép chuyển tiếp các database statements đến 1 hoặc nhiều MySQL/MariaDB database server
- Keepalived : sử dụng để cung cấp một single endpoint thông qua virtual IP address => cho phép IP address di chuyển giữa các load balancer nodes

## 5 - References
- https://severalnines.com/resources/whitepapers/galera-cluster-mysql-tutorial
- https://severalnines.com/blog/what-check-if-mysql-io-utilisation-high/
- https://severalnines.com/blog/how-deploy-percona-xtradb-cluster-8-high-availability/
- https://galeracluster.com/library/documentation/architecture.html
